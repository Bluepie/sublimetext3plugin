# Plugin for Sublime text-3

A C-prettify plugin for sublime text, just install it and use it.. No additional configurations or installs required.

![Demo](http://imdead.esy.es/img/functions.gif)

## Getting Started

Just copy the 'Cprettify' folder to  "C:\Users\'[username_here]'\AppData\Roaming\Sublime Text 3\Packages\" -for Windows

(or)

Install from Package Control

### Prerequisites

Install package control for sublime text..then proceed from there on

## Built With

* Python 
* Sublime-api - documented via comments
* Json - Menus and Settings

## Usage

#### NOTE! use user setting to override, if changes are made to default, updates will reset it.

* Override provided key bindings from settings if it interfears
* You can also specify which config file to use from provided configs 
(or) you can provide your own config from 'create config file' under settings
and paste your config there and set '"user_config_file":"False"' to '"user_config_file":"True"'
* Incase you messed up your config file use "restore default setting" to restore original settings.


## Authors

* **Gopinath (aka) Bluepie** * **Anand (aka) ThaniOruvan**

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
